package com.fora.foodapp.autho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.fora.foodapp.R;
import com.fora.foodapp.util.Utils;
import com.mukesh.OtpView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtpActivity extends AppCompatActivity {

    @BindView(R.id.otp_view)OtpView otp_view;
    String value="0";
    String phoneNumber="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.fullscreenView(OtpActivity.this);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
    }
    @OnClick({R.id.btnOtp})
    public  void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnOtp:


                Bundle bundle=getIntent().getExtras();
                if(bundle!=null)
                {
                    value=bundle.getString("customerType");
                    phoneNumber=bundle.getString("phoneNumber");
                }
                Intent i=new Intent(OtpActivity.this,SetPasswordActivity.class);
                i.putExtra("customerType",value);
                i.putExtra("phoneNumber",phoneNumber);
                i.putExtra("otp",otp_view.getOTP().toString());
                startActivity(i);


                break;

        }
    }

    public  void validationSignUp(View view)
    {

        if(TextUtils.isEmpty(otp_view.getOTP().toString()))
        {
            Utils.showErrorMessage(getApplicationContext(),"Please enter otp! ");
        }

        else
        {

        }
    }
}
