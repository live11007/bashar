package com.fora.foodapp.autho;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.util.Util;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.fora.foodapp.Dashboard;
import com.fora.foodapp.R;
import com.fora.foodapp.dashboard.DashboardMain;
import com.fora.foodapp.database.DatabaseAccess;
import com.fora.foodapp.database.User;
import com.fora.foodapp.util.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;



public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {public CallbackManager callbackManager;
    @BindView(R.id.btnSingUp)Button btnSingUp;
    @BindView(R.id.txtForgotPassword)TextView txtForgotPassword;
    @BindView(R.id.edtEmail)EditText edtEmail;
    @BindView(R.id.edtPassword)EditText edtPassword;
    @BindView(R.id.btnFacebook)Button btnFacebook;
    @BindView(R.id.btnLogin)Button btnLogin;
    GraphRequest request;
    private static final int RC_SIGN_IN = 9001;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.fullscreenView(LoginActivity.this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();




    }
    @OnClick({R.id.btnSingUp,R.id.btnLogin,R.id.btnFacebook,R.id.btnGoogle,R.id.txtForgotPassword})
    public  void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnSingUp:
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                break;

            case R.id.txtForgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
                break;
            case R.id.btnLogin:
                validationSignUp(view);
                break;
            case R.id.btnFacebook:
                loginFb();
                break;
            case R.id.btnGoogle:
                checkAndRequestPermissions();
                break;

        }
    }

    public  void validationSignUp(View view)
    {

        if(TextUtils.isEmpty(edtEmail.getText().toString()))
        {
            Utils.showErrorMessage(getApplicationContext(),"Please enter email id ! ");
        }
        else if(TextUtils.isEmpty(edtPassword.getText().toString()))
        {
            Utils.showErrorMessage(getApplicationContext(),"Please enter password!");
        }


        else
        {
            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
            User currentUser = databaseAccess.Authenticate(new User(null, null, edtEmail.getText().toString(), edtPassword.getText().toString()));
            if (currentUser != null) {
                Cursor res = databaseAccess.getAllLoginData(currentUser.getUserName());
                if(res.getCount() == 0) {
                    return;
                }
                while (res.moveToNext()) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt("islogin", 1);
                    editor.putInt("TenentID", res.getInt(0));
                    editor.putInt("ContactMyID", res.getInt(1));
                    editor.putString("PersName3", res.getString(6));
                    editor.putString("EMAIL1", res.getString(9));
                    editor.putString("MOBPHONE", res.getString(10));
                    editor.putString("ADDR1", res.getString(12));
                    editor.putString("ADDR2", res.getString(13));
                    editor.commit();

                }

                Utils.showErrorMessage(getApplicationContext(),"Successfully Logged in!");
                startActivity(new Intent(LoginActivity.this, DashboardMain.class).setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();


            } else {
                Utils.showErrorMessage(getApplicationContext(),"Failed to log in , please try again");
                }



        }
    }

    private void loginFb() {
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }

        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, (Arrays.asList("public_profile", "email")));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AccessToken accessToken = loginResult.getAccessToken();
                        Profile profile = Profile.getCurrentProfile();
                        request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {
                                            String fb_id = object.getString("id");
                                            String fb_name = object.getString("name");
                                            String fb_email = object.getString("email");
                                            String image = new URL("https://graph.facebook.com/" + loginResult.getAccessToken().getUserId() + "/picture?type=large").toString();


                                            Intent i=new Intent(LoginActivity.this,DashboardMain.class);
                                            i.putExtra(Utils.NAME,fb_name);
                                            i.putExtra(Utils.EMAIL,fb_email);
                                            i.putExtra(Utils.PROFILE,image);
                                            i.putExtra(Utils.GOOGLEID,fb_id);

                                            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
                                            databaseAccess.open();
                                            User user=new User();
                                            user.setEmail(fb_email);
                                            user.setPhone(fb_email);
                                            user.setContactMyID(39);
                                            user.setFb_token_id(fb_id);
                                            databaseAccess.addLibrary(user);
                                            databaseAccess.close();
                                            startActivity(i);
                                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                            SharedPreferences.Editor editor = pref.edit();
                                            editor.putInt("islogin", 1);
                                            editor.putString("image", image);
                                            editor.putString("PersName3", fb_name);
                                            editor.putString("EMAIL1", fb_email);
                                            editor.commit();


                                        } catch (Exception e) {
                                            Log.e("Exce", "" + e.toString());
                                        }
                                    }
                                });
                        Bundle parameters1 = new Bundle();
                        parameters1.putString("fields", "id,name,email");
                        request.setParameters(parameters1);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        LoginManager.getInstance().logOut();
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e("Exce", "" + exception.toString());
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                    }
                }
        );
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    private boolean checkAndRequestPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.GET_ACCOUNTS)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, 1000);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, 1000);
            }
        } else {

            loginGoogle();

        }
        return true;
    }
    private void loginGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String google_id = acct.getId();
            String google_name = acct.getDisplayName();
            String google_email = acct.getEmail();
            String google_image = String.valueOf(acct.getPhotoUrl());
            Intent i=new Intent(LoginActivity.this,SignUpActivity.class);
            i.putExtra(Utils.NAME,google_name);
            i.putExtra(Utils.EMAIL,google_email);
            i.putExtra(Utils.PROFILE,google_image);
            i.putExtra(Utils.GOOGLEID,google_id);
            startActivity(i);
            startActivity(new Intent(LoginActivity.this, DashboardMain.class).setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("islogin", 1);
            editor.commit();

        } else {


            Toast.makeText(this, "Login failed." + result.toString(), Toast.LENGTH_SHORT).show();
        }
    }

}

