package com.fora.foodapp.autho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.fora.foodapp.Dashboard;
import com.fora.foodapp.R;
import com.fora.foodapp.checkout.CheckOutActivity;
import com.fora.foodapp.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity {
    @BindView(R.id.edtPhone)EditText edtPhone;
    @BindView(R.id.linerShowAddress)LinearLayout linerShowAddress;
    @BindView(R.id.txtAddress)TextView txtAddress;
    String value="1";
    int countId=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.fullscreenView(SignUpActivity.this);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);


        linerShowAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(SignUpActivity.this, linerShowAddress);
                popup.getMenuInflater().inflate(R.menu.menu_dr_type, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId())
                        {
                            case  R.id.nav_driver:
                                value="2";

                                break;
                            case  R.id.nav_doctor:
                                value="1";

                                break;
                            case  R.id.nav_customer:
                                value="9";

                                break;
                        }
                        txtAddress.setText(item.getTitle());

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
    }
    @OnClick({R.id.btnGetOtp})
    public  void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnGetOtp:
                validationSignUp(view);
                break;

        }
    }

    public  void validationSignUp(View view)
    {

        if(TextUtils.isEmpty(edtPhone.getText().toString()))
        {
            Utils.showErrorMessage(getApplicationContext(),"Please enter phone number! ");
        }
        else if(!Patterns.PHONE.matcher(edtPhone.getText().toString()).matches()  )
        {
            Utils.showErrorMessage(this,"Please enter valid email address !");
        }

        else
        {
            Intent i=new Intent(SignUpActivity.this,OtpActivity.class);
            i.putExtra("customerType",value);
            i.putExtra("phoneNumber",edtPhone.getText().toString());
            startActivity(i);
        }
    }
}
