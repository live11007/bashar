package com.fora.foodapp.autho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fora.foodapp.Dashboard;
import com.fora.foodapp.R;
import com.fora.foodapp.dashboard.DashboardMain;
import com.fora.foodapp.database.DatabaseAccess;
import com.fora.foodapp.database.User;
import com.fora.foodapp.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetPasswordActivity extends AppCompatActivity {

    @BindView(R.id.edtNewPassword)EditText edtNewPassword;
    @BindView(R.id.edtConfirmPassword)EditText edtConfirmPassword;
    String value="0";
    String phoneNumber="0";
    String otp="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.fullscreenView(SetPasswordActivity.this);
        setContentView(R.layout.activity_set_password);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnDone})
    public  void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnDone:
                validationSignUp(view);
                break;

        }
    }

    public  void validationSignUp(View view)
    {

        if(TextUtils.isEmpty(edtNewPassword.getText().toString()))
        {
            Utils.showErrorMessage(getApplicationContext(),"Please enter new password! ");
        }
       else  if(TextUtils.isEmpty(edtConfirmPassword.getText().toString()))
        {
            Utils.showErrorMessage(getApplicationContext(),"Please enter confirm  password! ");
        }
        else  if(!edtNewPassword.getText().toString().equalsIgnoreCase(edtConfirmPassword.getText().toString()))
        {
            Utils.showErrorMessage(getApplicationContext(),"Password not match");
        }

        else
        {


            Bundle bundle=getIntent().getExtras();
            if(bundle!=null)
            {
                value=bundle.getString("customerType");
                phoneNumber=bundle.getString("phoneNumber");
                otp=bundle.getString("otp");
            }

            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
            databaseAccess.open();
            User user=new User();
            user.setEmail(phoneNumber);
            user.setPhone(phoneNumber);
            user.setContactMyID(2);
            user.setContacType(value);
            user.setPassword(edtNewPassword.getText().toString());
            databaseAccess.addLibrary(user);

            databaseAccess.close();

            Intent intent = new Intent(SetPasswordActivity.this, DashboardMain.class);
            intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

        }
    }
}
